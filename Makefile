# Configuration

CC		= gcc
LD		= gcc
AR		= ar
CFLAGS		= -g -std=gnu99 -Wall -Iinclude -fPIC
LDFLAGS		= -Llib -pthread
ARFLAGS		= rcs

# Variables

CLIENT_HEADERS  = $(wildcard include/mq/*.h)
CLIENT_SOURCES  = $(wildcard src/client/*.c)
CLIENT_OBJECTS  = $(CLIENT_SOURCES:.c=.o)
CLIENT_LIBRARY  = lib/libmq_client.a
CLIENT_PROGRAM  = bin/mq_client

TEST_SOURCES    = $(wildcard src/tests/test_*.c)
TEST_OBJECTS    = $(TEST_SOURCES:.c=.o)
TEST_PROGRAMS   = $(subst src/tests,bin,$(basename $(TEST_OBJECTS)))

# Rules

all:	$(CLIENT_LIBRARY)

%.o:			%.c $(CLIENT_HEADERS)
	@echo "Compiling $@"
	@$(CC) $(CFLAGS) -c -o $@ $<

$(CLIENT_LIBRARY):	$(CLIENT_OBJECTS)
	@echo "Linking   $@"
	@$(AR) $(ARFLAGS) $@ $^

bin/%:  		src/tests/%.o $(CLIENT_LIBRARY)
	@echo "Linking   $@"
	@$(LD) $(LDFLAGS) -o $@ $^

test:   		$(TEST_PROGRAMS)
	@echo "Testing   Queue (Unit)"
	@for i in $$(seq $$(./bin/test_queue_unit 2>&1 | tail -n 1 | awk '{print $$1}')); do \
	    valgrind --leak-check=full ./bin/test_queue_unit $$i > tests.log 2>&1; \
	    grep -q 'ERROR SUMMARY: 0' tests.log || cat tests.log; \
	    ! grep -q 'Assertion' tests.log || cat tests.log; \
	done

	@echo "Testing   Queue (Functional)"
	@valgrind --leak-check=full ./bin/test_queue_functional > tests.log 2>&1
	@grep -q 'ERROR SUMMARY: 0' tests.log || cat tests.log
	@! grep -q 'Assertion' tests.log || cat tests.log

	@echo "Testing   Echo Client"
	@./bin/test_echo_client > tests.log 2>&1

clean:
	@echo "Removing  objects"
	@rm -f $(CLIENT_OBJECTS)

	@echo "Removing  libraries"
	@rm -f $(CLIENT_LIBRARIES)

	@echo "Removing  test programs"
	@rm -f $(TEST_PROGRAMS)

.PRECIOUS: %.o
